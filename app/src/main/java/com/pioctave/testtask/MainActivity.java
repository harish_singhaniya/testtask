package com.pioctave.testtask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pioctave.pibell.pibellsdk.PiVideoCall.VideoCallActivity;
import com.pioctave.pibell.pibellsdk.api.APICallBack;
import com.pioctave.pibell.pibellsdk.api.ApiException;
import com.pioctave.pibell.pibellsdk.api.Configuration;
import com.pioctave.pibell.pibellsdk.api.http.client.HttpContext;
import com.pioctave.pibell.pibellsdk.auth.AuthController;
import com.pioctave.pibell.pibellsdk.auth.AuthCredentials;
import com.pioctave.pibell.pibellsdk.auth.Token;
import com.pioctave.pibell.pibellsdk.device.Device;
import com.pioctave.pibell.pibellsdk.device.DeviceAddition.AddNewDeviceActivity;
import com.pioctave.pibell.pibellsdk.device.DeviceController;
import com.pioctave.pibell.pibellsdk.device.DeviceOnline;
import com.pioctave.pibell.pibellsdk.device.Settings.MotionSelectionActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TextView txtAddDevice,txtGetDeviceList,txtMotionSetting,txtVideoCall,txtDeviceAvailable;
    private List<Device> devices;
    private EditText edtUserName,edtPassword;
    private Button btnOk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(!Configuration.getInitializeRequire()) {
            Configuration.initialize(getApplicationContext(),getResources().getResourceEntryName(R.drawable.tron_xthings_dark) ,
                    getResources().getResourceEntryName(R.drawable.tron_sthings_transparent),"NTY4M2FhMzNlNTY0ZDYxNDI1YjI5Y2IxOiQyYSQxMCR4Rm1Qdzc0NE11SGpkNXFHMGhXOHR1QVhUSUFlZlpuS2dKRHlXOVI1RE9xLk94V1A2M2RXYQ==");
        }
        devices = new ArrayList<Device>();
        txtAddDevice = (TextView)findViewById(R.id.txtAddNewDevice);
        txtGetDeviceList = (TextView)findViewById(R.id.txtGetDeviceList);
        txtMotionSetting = (TextView)findViewById(R.id.txtMotionSetting);
        txtVideoCall = (TextView)findViewById(R.id.txtVideoCall);
        txtDeviceAvailable = (TextView)findViewById(R.id.txtDeviceAvailable);
        edtUserName = (EditText) findViewById(R.id.edtUserName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnOk = (Button)findViewById(R.id.btnOk);


        txtAddDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addNewDevice = new Intent(MainActivity.this, AddNewDeviceActivity.class);
                startActivity(addNewDevice);
            }
        });


        txtGetDeviceList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceList();
            }
        });

        txtMotionSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(devices!=null && !devices.isEmpty() && devices.size()>0) {
                    Intent motionSetting = new Intent(MainActivity.this, MotionSelectionActivity.class);
                    motionSetting.putExtra(getResources().getString(R.string.device), devices.get(0));
                    startActivity(motionSetting);
                }else{
                    Toast.makeText(MainActivity.this, "No device found",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAccessToken();
            }
        });

        txtVideoCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(devices!=null && !devices.isEmpty() && devices.size()>0) {
                    openVideoCallActivity();
                }else{
                    Toast.makeText(MainActivity.this, "No device found",Toast.LENGTH_SHORT).show();
                }
            }
        });

        txtDeviceAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(devices!=null && !devices.isEmpty() && devices.size()>0) {
                    getDeviceAvailable();
                }else{
                    Toast.makeText(MainActivity.this, "No device found",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void openVideoCallActivity() {
        Intent nextActivity = new Intent(MainActivity.this, VideoCallActivity.class);
        nextActivity.putExtra(getString(R.string.device_id), devices.get(devices.size()-1).getId());
        nextActivity.putExtra(getString(R.string.device), devices.get(devices.size()-1));
        nextActivity.putExtra(getString(R.string.from_caps), "device_information");
        nextActivity.putExtra(getString(R.string.last_image), devices.get(devices.size()-1).getLastImage());
        nextActivity.putExtra(getString(R.string.event_last_image), "");
        nextActivity.putExtra(getString(R.string.builtConfigForSdk), BuildConfig.DEBUG);
        nextActivity.putExtra(getString(R.string.builtVersionNameForSdk), BuildConfig.VERSION_NAME);
        nextActivity.putExtra(VideoCallActivity.EXTRA_TO, devices.get(devices.size()-1).getId());
        nextActivity.putExtra(VideoCallActivity.EXTRA_TO_TYPE, getString(R.string.device_small));
        nextActivity.putExtra(VideoCallActivity.EXTRA_VIDEOCODEC, getString(R.string.video_codec_h264));
        nextActivity.putExtra(VideoCallActivity.EXTRA_MEDIA_START_ENABLE, "false");
        if (devices.get(devices.size()-1).isSDESSupport()) {
            nextActivity.putExtra(VideoCallActivity.EXTRA_ENABLE_DTLS_SRTP_KEY_AGREEMENT, true);
        }
        if (devices.get(devices.size()-1).isICECacheSupport()) {
            nextActivity.putExtra(VideoCallActivity.EXTRA_ENABLE_ICE_CACHE, true);
        }
        startActivity(nextActivity);
    }

    private void getDeviceList(){
        try {
            DeviceController.getInstance(MainActivity.this).getDevices(true, new APICallBack<List<Device>>() {
                @Override
                public void onSuccess(HttpContext context, List<Device> response) {
                    devices = response;
                    Toast.makeText(MainActivity.this, "Get Device List successful List size : "+devices.size(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(HttpContext context, Throwable error) {
                    Toast.makeText(MainActivity.this, "Get Device List Failed",Toast.LENGTH_SHORT).show();
                }
            });
        } catch (ApiException e) {
            Toast.makeText(MainActivity.this, "Get Device List Exception",Toast.LENGTH_SHORT).show();
        }
    }

    private void getAccessToken(){
        if(edtUserName.getText().toString().length()<=0 && edtPassword.getText().toString().length()<=0){
            Toast.makeText(MainActivity.this, "User Name and Password not be empty",Toast.LENGTH_SHORT).show();
        }else {
            AuthCredentials credentials = new AuthCredentials(edtUserName.getText().toString(), edtPassword.getText().toString());
            AuthController.getInstance(MainActivity.this).getToken(credentials, new APICallBack<Token>() {
                @Override
                public void onSuccess(HttpContext context, Token response) {
                    Toast.makeText(MainActivity.this, "Login successful",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(HttpContext context, Throwable error) {
                    Toast.makeText(MainActivity.this, "Login failed",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void getDeviceAvailable(){
        try {
            DeviceController.getInstance(this).getDeviceAvailable(devices.get(0).getId(), new APICallBack<DeviceOnline>() {
                @Override
                public void onSuccess(HttpContext httpContext, DeviceOnline deviceOnline) {
                    Toast.makeText(MainActivity.this, "Device Available : "+deviceOnline.getDeviceOnline(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(HttpContext httpContext, Throwable throwable) {
                    Toast.makeText(MainActivity.this, "Device Available failed",Toast.LENGTH_SHORT).show();
                }
            });
        } catch (ApiException e) {
            Toast.makeText(MainActivity.this, "Device Available",Toast.LENGTH_SHORT).show();
        }
    }


}
